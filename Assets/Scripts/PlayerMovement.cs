﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR;

public class PlayerMovement : MonoBehaviour
{
    public Transform head;
    public float speed;
    [Range(0f, 1f)]
    public float deadzone;

    private CharacterController controller;
    private Vector2 inputDirection;
    private Vector3 velocity;
    private Vector3 orientationForward;
    private Vector3 orientationRight;

    private void Awake()
    {
        controller = GetComponent<CharacterController>();
    }

    private void FixedUpdate()
    {
        velocity += Physics.gravity * Time.fixedDeltaTime;
        velocity.x = 0f;
        velocity.z = 0f;

        velocity += orientationForward * inputDirection.y * speed;
        velocity += orientationRight * inputDirection.x * speed;

        controller.Move(velocity * Time.fixedDeltaTime);

        if (controller.isGrounded && velocity.y < 0f)
        {
            velocity.y = 0f;
        }
    }

    private void SetOrientation()
    {
        orientationForward = head.forward;
        orientationForward.y = 0f;
        orientationForward = orientationForward.normalized;

        orientationRight = head.right;
        orientationRight.y = 0f;
        orientationRight = orientationRight.normalized;
    }

    public void Move(Vector2 direction)
    {
        if (direction.sqrMagnitude < deadzone * deadzone)
        {
            inputDirection = Vector2.zero;
        }
        else
        {
            if (inputDirection.sqrMagnitude < deadzone * deadzone)
            {
                SetOrientation();
            }
            inputDirection = direction;
        }
    }
}
