﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cart : MonoBehaviour
{
    public WayPoint startPoint;
    public float speed;
    public float deceleration;
    public float breakStrength;
    public bool isMovingForward = true;


    private WayPoint.WayPointAgent agent;
    private float velocity;
    private float breakValue;


    // Start is called before the first frame update
    void Start()
    {
        agent = startPoint.GetAgent();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        agent.Move(velocity * Time.fixedDeltaTime).ApplyOrientation(transform);
        velocity = Mathf.MoveTowards(velocity, 0f, (deceleration + breakValue) * Time.fixedDeltaTime);
    }

    public void Boost(float amount)
    {
        amount = Mathf.Clamp01(amount);
        float value = amount * speed * (isMovingForward ? 1f : -1f);
        if (isMovingForward ? velocity < 0f : velocity > 0f)
        {
            velocity = Mathf.MoveTowards(velocity, value, breakStrength * Time.deltaTime);
        }
        else
        {
            velocity = Mathf.Max(velocity, value);
        }
    }

    public void Break(float value)
    {
        value = Mathf.Clamp01(value);
        breakValue = value * breakStrength;
    }
}
