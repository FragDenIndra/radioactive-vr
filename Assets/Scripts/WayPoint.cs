﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WayPoint : MonoBehaviour
{
    public WayPoint next;
    public WayPoint prev;

    public Transform leftTangent, rightTangent;

    private float distance = 0f;

    private void OnDrawGizmos()
    {
        Gizmos.DrawLine(transform.position, leftTangent.position);
        Gizmos.DrawLine(transform.position, rightTangent.position);
        Gizmos.color = Color.red;
        if (next)
        {
            Vector3 current = transform.position;
            Vector3 n;
            for (float progress = 0f; progress <= 0.5f; progress += 0.05f)
            {
                n = BazierBetween(transform.position, rightTangent.position, next.leftTangent.position, next.transform.position, progress).position;
                Gizmos.DrawLine(current, n);
                current = n;
            }
        }
        if (prev)
        {
            Vector3 current = transform.position;
            Vector3 n;
            for (float progress = 0f; progress <= 0.5f; progress += 0.05f)
            {
                n = BazierBetween(transform.position, leftTangent.position, prev.rightTangent.position, prev.transform.position, progress).position;
                Gizmos.DrawLine(current, n);
                current = n;
            }
        }
    }

    private static Orientation BazierBetween(Vector3 a, Vector3 b, Vector3 c, Vector3 d, float p)
    {
        Vector3 ab = Vector3.Lerp(a, b, p);
        Vector3 cd = Vector3.Lerp(c, d, p);
        Vector3 bc = Vector3.Lerp(b, c, p);
        Vector3 ab_bc = Vector3.Lerp(ab, bc, p);
        Vector3 bc_cd = Vector3.Lerp(bc, cd, p);
        return new Orientation(Vector3.Lerp(ab_bc, bc_cd, p), bc_cd - ab_bc);
    }

    private void Awake()
    {
        if (next)
        {
            distance = Vector3.Distance(transform.position, next.transform.position);
        }
    }

    public WayPointAgent GetAgent() => new WayPointAgent(this, 0f);

    public class WayPointAgent
    {
        private WayPoint currentPoint;
        private float currentDistance;


        public WayPointAgent(WayPoint currentPoint, float currentDistance, float agentLength)
        {
            this.currentPoint = currentPoint;
            this.currentDistance = currentDistance;
        }
        public WayPointAgent(WayPoint currentPoint, float currentDistance) : this(currentPoint, currentDistance, 0f) { }

        public Orientation Move(float distance)
        {
            currentDistance += distance;
            while (currentDistance >= currentPoint.distance)
            {
                if (currentPoint.next)
                {
                    currentDistance -= currentPoint.distance;
                    currentPoint = currentPoint.next;
                }
                else
                {
                    currentDistance = currentPoint.distance;
                    break;
                }
            }
            while (currentDistance < 0f)
            {
                if (currentPoint.prev)
                {
                    currentPoint = currentPoint.prev;
                    currentDistance += currentPoint.distance;
                }
                else
                {
                    currentDistance = 0f;
                    break;
                }
            }
            return BazierBetween(currentPoint.transform.position, currentPoint.rightTangent.position, currentPoint.next.leftTangent.position, currentPoint.next.transform.position, currentDistance / currentPoint.distance);
        }
    }

    public struct Orientation
    {
        public Vector3 position;
        public Vector3 forward;

        public Orientation(Vector3 position, Vector3 forward)
        {
            this.position = position;
            this.forward = forward;
        }

        public void ApplyOrientation(Transform transform)
        {
            transform.position = position;
            transform.forward = forward;
        }
    }
}
