﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestScript : MonoBehaviour
{

    public float ScalerStep = 0.5f;
    Vector3 currentScale;

    // Start is called before the first frame update
    void Start()
    {
        currentScale = this.gameObject.transform.localScale;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown("d"))
        {
            currentScale = new Vector3(currentScale.x - ScalerStep, currentScale.y - ScalerStep, currentScale.z - ScalerStep);

            scaleDown(currentScale);
        }

        if (Input.GetKeyDown("u"))
        {
            currentScale = new Vector3(currentScale.x + ScalerStep, currentScale.y + ScalerStep, currentScale.z + ScalerStep);
            scaleUp(currentScale);
        }

    }


    public void scaleDown (Vector3 scaleStep)
    {
        this.gameObject.transform.localScale = scaleStep;
    }

    public void scaleUp (Vector3 scaleStep)
    {
        this.gameObject.transform.localScale = scaleStep;
    }


}
