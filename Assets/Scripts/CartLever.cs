﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class CartLever : MonoBehaviour
{
    public float maxAcceleration;
    public Transform join;
    public float deadZone;

    public FloatEvent onBoost;

    private float activeAngle;
    private float currentAngle;
    private bool upMovement;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        float deltaAngle = join.eulerAngles.z - currentAngle;
        float scaledDelta = deltaAngle / Time.fixedDeltaTime;
        currentAngle = join.eulerAngles.z;

        scaledDelta = Mathf.Clamp(scaledDelta, -maxAcceleration, maxAcceleration);

        if (scaledDelta < 0f != upMovement)
        {
            float activeDelta = activeAngle - currentAngle;
            if (Mathf.Abs(activeDelta) >= deadZone)
            {
                activeAngle = currentAngle;
                upMovement = scaledDelta < 0f;
                onBoost.Invoke(Mathf.Abs(scaledDelta) / maxAcceleration);
            }
        }
        else
        {
            activeAngle = currentAngle;
            onBoost.Invoke(Mathf.Abs(scaledDelta) / maxAcceleration);
        }
    }

    [System.Serializable]
    public class FloatEvent : UnityEvent<float> { }
}
