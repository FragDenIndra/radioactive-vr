﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerHeightController : MonoBehaviour
{
    public Transform head;

    private CharacterController controller;


    private void Awake()
    {
        controller = GetComponent<CharacterController>();
    }

    // Update is called once per frame
    void Update()
    {
        controller.height = head.position.y - transform.position.y - controller.radius;
    }
}
