﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using Valve.VR;

public class PlayerInputs : MonoBehaviour
{
    public SteamVR_Input_Sources hand;

    public Vector2Event onMove;


    // Update is called once per frame
    void Update()
    {
        onMove.Invoke(SteamVR_Actions._default.MovementAxis.GetAxis(hand));
    }


    [System.Serializable]
    public class Vector2Event : UnityEvent<Vector2> { }
}
