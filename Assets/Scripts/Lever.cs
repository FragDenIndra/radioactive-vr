﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Lever : MonoBehaviour
{
    public Transform lever;
    public HingeJoint join;

    public float min;
    public float max;
    public float settleValue;
    public float settleAcceleration;

    public FloatEvent onValueChange;


    private bool isGrabbed = false;
    private bool settleAtValue;
    private float settleVelocity;
    private float value;


    private void Awake()
    {
        settleAtValue = max > min ? max >= settleValue && min <= settleValue : max <= settleValue && min >= settleValue;
    }

    // Update is called once per frame
    void Update()
    {
        float relativeValue = (lever.eulerAngles.x - join.limits.min) / join.limits.contactDistance;
        float value = (max - min) * relativeValue + min;
        if (!isGrabbed)
        {
            float settleTarget = GetSettleTarget();
            if (settleTarget != lever.eulerAngles.x)
            {
                settleVelocity += settleAcceleration * Time.deltaTime;
                Vector3 euler = lever.eulerAngles;
                euler.x = Mathf.MoveTowards(lever.eulerAngles.x, settleTarget, settleVelocity * Time.deltaTime);
                lever.eulerAngles = euler;
            }
            else
            {
                settleVelocity = 0f;
            }
        }
        if (this.value != value)
        {
            this.value = value;
            onValueChange.Invoke(value);
        }
    }

    public void Grab() => isGrabbed = true;
    public void Release() => isGrabbed = false;

    private float GetSettleTarget()
    {
        if (settleAtValue)
        {
            return settleValue;
        }
        else
        {
            if (Mathf.Abs(min - value) <= Mathf.Abs(max - value))
            {
                return min;
            }
            else
            {
                return max;
            }
        }
    }


    [System.Serializable]
    public class FloatEvent : UnityEvent<float> { }
}
