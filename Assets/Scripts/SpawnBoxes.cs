﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnBoxes : MonoBehaviour
{

    public Transform spawnPoint;
    public GameObject boxPrefab;

    public float boxScale = 1.0f;
    public int boxesAmount = 9;
    private Transform currentPosition;

    // Start is called before the first frame update
    void Start()
    {
        currentPosition = spawnPoint;

        for (int i = 0; i < boxesAmount; i++)
        {
            if (i%3 == 0)
            {
                for (int x = 0; x/3 < boxesAmount/3; x++)
                {
                    Instantiate(boxPrefab, currentPosition, true);
                    currentPosition.position = new Vector3(currentPosition.position.x - boxScale * 2 * 3, currentPosition.position.y + boxScale * 2, currentPosition.position.z);
                }
            }
            Instantiate(boxPrefab, currentPosition, true);
            currentPosition.position = new Vector3(currentPosition.position.x + boxScale*2, currentPosition.position.y, currentPosition.position.z);
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
